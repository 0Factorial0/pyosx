def start():

    import os
    import time
        
    try:
        from osx.user_management.login import login
        from osx.user_management.register import register

        from osx.systemx.additional_functions.clear import clear
        from osx.systemx.additional_functions.install_dependencies import install_dependencies
    except:
        print("Import Error.")

    #install dependencies
    install_dependencies()

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #check if registered
    isRegistered_file = open(file_path+"/osx/user_management/user0/isRegistered.txt", "r")
    isRegistered = isRegistered_file.read()

    isRegistered_file.close()

    #if user is registered go to login page, or not go to register page
    if isRegistered == "1":
        login()
    elif isRegistered == "0":
        register()
    else:
        print("User Management Error.")
        time.sleep(2)
        start()

start()
