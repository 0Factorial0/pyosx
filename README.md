# Proje ¿-DOS

Kendi programları olan ve dışarıdan program ekleyip çalıştırılabilen, sıfırdan vanilla python ile yapılmış bir işletim sistemi projesi.

Projenin seçilme sebebi: Programlama'nın neredeyse her dalında yetkin hâle gelene kadar üzerine uğraşılabilecek ve eklenebilecek bir proje.

# Nasıl Kurulur?
1. Git kütüphanesini bilgisayarınıza kurun: https://git-scm.com/downloads
2. Python3'ü bilgisayarınıza kurun: https://www.python.org/downloads/
   - Ya da Homebrew kullanın: https://docs.brew.sh/Installation
   - Terminalinizden `brew install python` yazarak kurabilirsiniz. 
3. CMD, Git Bash, Terminator, iTerm, Guake, Zsh veya herhangi bir terminali açın.
4. Aşağıdaki komutları kullanarak gitlab hesabınızı bağlayın:
   - `git config --global user.name "<username>"`
   - `git config --global user.email "<e-mail>"`
5. `git clone https://gitlab.com/0Factorial0/pyosx.git`'ile projeyi klonlayın.
6. `cd <file_name>`, `cd ..` komutlarıyla projenin içine gidin.
7. `python3 start.py` komutuyla programı başlatın.

# Projenin Güncel İçeriği;

## Özellikler:
- Dosya Sistemi [WIP]
- Kayıt-Giriş [v1.0]
- Kullanıcı Arayüzü [v0.1]
- Sistemin İçinde Dolaşmak İçin Menüler [v1.0]

## Öz Uygulamalar:
- Saat ve Tarih Uygulaması [v1.0]
- Hesap Makinesi [v2.0]
- Not Alma Uygulaması [v0.1]
- Kronometre [v1.0]
- Kullanıcı Ayarları [v0.2]

## İkinci Parti Uygulamalar:
   ### Mini Oyunlar
   - Sayı Tahmin Oyunu [v1.0]
   - Yazı Tura Oyunu [v1.0]
   - Taş Kağıt Makas Oyunu [v1.0]
   - Matematik Oyunu [v0.1]
   - Monthy Hall Oyunu [v1.0]
   - Yemek Toplama Oyunu [v1.0]

   ### Oyunlar
   - Wish You Not [alpha0.1]
   - Platform Oyunu [alpha0.0]

   ### Deneyler
   - Sıralama Algoritmaları [v1.0]
   - Yapay Zeka Yarıştırma Simülasyonu [WIP]

<hr>

#### Emeği Geçenler:
- 0x0
- k1esko
- efecaglar
- xpN7
- mohurine
- meowtastic23

<hr>

//end
