def notepad():
    
    import os
    import time

    from apps.menu import menu

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    #menu
    print("--------------------------")
    print("PYX Notepad")
    print("--------------------------")

    #default variables
    isUserWriting = True
    row_count = 1
    line_list = []

    #get user input
    while isUserWriting == True:
        userinput0 = str(input("Write Line [{0}]: ".format(row_count)))
        #if //end is written it's finish line
        if userinput0 == "//end":
            isUserWriting = False
        #if //menu is written break and go to menu
        elif userinput0 == "//menu":
            menu()
        else:
            row_count = row_count + 1
            line_list.append(userinput0)

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))
    file_name = "note0.txt"

    #write the lines
    file = open(file_path+"/apps/internal_apps/notes/"+file_name, "w")
    for line in line_list:
        file.write("%s\n" % line)
    file.close()

    #return notepad menu
    time.sleep(2)
    notepad()
