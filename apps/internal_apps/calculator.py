#add math as global
from math import *

def calculator():
    
    calculator_menu()

#calculator menu function
def calculator_menu():

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    #calculator menu
    print("--------------------------")
    print("Calculator")
    print("--------------------------")
    print("Press 1 For Addition")
    print("Press 2 For Substraction")
    print("Press 3 For Multiplication")
    print("Press 4 For Division")
    print("Press 100 For Scientific Functions")
    print("Press 200 For Temperature Translation")
    print("Press 300 For Angle Translation")
    print("Press 1000 For Write Your Function")
    print("--------------------------")
    print("Press 0 For Main Menu")
    print("--------------------------")

    solution0 = 0

    #get input
    try:
        userinput0 = int(input("Input: "))
        
        from apps.menu import menu

        #clear screen
        clear()
        
        if userinput0 == 1:
            addition0(solution0)
        elif userinput0 == 2:
            substraction0(solution0)
        elif userinput0 == 3:
            multiplication0(solution0)
        elif userinput0 == 4:
            division0(solution0)
        elif userinput0 == 100:
            scientific_calculator_menu()
        elif userinput0 == 200:
            temperature_translator()
        elif userinput0 == 300:
            angle_translator()
        elif userinput0 == 1000:
            write_your_function()
        elif userinput0 == 0:
            menu()
        else:
            calculator_menu()
    #if catched error return menu
    except:
        calculator_menu()

#scientific calculator menu function
def scientific_calculator_menu():

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    #scientific calculator menu
    print("--------------------------")
    print("Scientific Calculator")
    print("--------------------------")
    print("Press 1 For Square")
    print("Press 2 For Square Root")
    print("Press 3 For Modulus")
    print("Press 4 For Exponent")
    print("Press 5 For Logarithm")
    print("Press 100 For Calculator Menu")
    print("Press 200 For Temperature Translation")
    print("Press 300 For Angle Translation")
    print("Press 1000 For Write Your Function")
    print("--------------------------")
    print("Press 0 For Main Menu")
    print("--------------------------")

    solution0 = 0

    #get input
    try:
        userinput0 = int(input("Input: "))
        
        from apps.menu import menu
        
        #clear screen
        clear()
        
        if userinput0 == 1:
            square0(solution0)
        elif userinput0 == 2:
            squareroot0(solution0)
        elif userinput0 == 3:
            modulus0(solution0)
        elif userinput0 == 4:
            exponent0(solution0)
        elif userinput0 == 5:
            logarithm0(solution0)
        elif userinput0 == 100:
            calculator_menu()
        elif userinput0 == 200:
            temperature_translator()
        elif userinput0 == 300:
            angle_translator()
        elif userinput0 == 1000:
            write_your_function()
        elif userinput0 == 0:
            menu()
        else:
            calculator_menu()
    #if catched error return menu
    except:
        calculator_menu()

#temperature translation menu
def temperature_translator():

    import time

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    #temperature menu
    print("--------------------------")
    print("Temperature Translator")
    print("--------------------------")

    #first unit
    print("Choose First Unit")
    print("--------------------------")
    print("Press 1 For Celsius (°C)")
    print("Press 2 For Fahrenheit (°F)")
    print("Press 3 For Kelvin (°K)")
    print("--------------------------")

    #get first input
    try:
        userinput0 = int(input("Input: "))
        if userinput0 < 1 or userinput0 > 3:
            print("--------------------------")
            print("Input Error.")
            time.sleep(3)
            temperature_translator()
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(3)
        temperature_translator()

    #clear screen
    clear()

    #temperature menu
    print("--------------------------")
    print("Temperature Translator")
    print("--------------------------")

    #second unit
    print("Choose Second Unit")
    print("--------------------------")
    if userinput0 != 1:
        print("Press 1 For Celsius (°C)")
    if userinput0 != 2:
        print("Press 2 For Fahrenheit (°F)")
    if userinput0 != 3:
        print("Press 3 For Kelvin (°K)")
    print("--------------------------")

    #get second input
    try:
        userinput1 = int(input("Input: "))
        if userinput1 < 1 or userinput1 > 3:
            print("--------------------------")
            print("Input Error.")
            time.sleep(3)
            temperature_translator()
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(3)
        temperature_translator()

    #clear screen
    clear()

    #temperature menu
    print("--------------------------")
    print("Temperature Translator")
    print("--------------------------")

   #integer to translate
    print("Input Number To Translate")
    print("--------------------------")
    
    #get number
    try:
        userinput2 = float(input("Input: "))
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(3)
        temperature_translator()

    #clear screen
    clear()

    #list of units
    units = [0, "Celsius (°C)", "Fahrenheit (°F)", "Kelvin (°K)"]
    solution0 = userinput2

    #combination of 3
    if userinput0 == userinput1:
        pass
    #celsius to fahrenheit
    elif userinput0 == 1 and userinput1 == 2:
        solution0 = (userinput2*1.8)+32
    #celsius to kelvin
    elif userinput0 == 1 and userinput1 == 3:
        solution0 = userinput2+273.15
    #fahrenheit to celsius
    elif userinput0 == 2 and userinput1 == 1:
        solution0 = (userinput2-32)/1.8
    #fahrenheit to kelvin
    elif userinput0 == 2 and userinput1 == 3:
        fahrenheit_to_celsius = (userinput2-32)/1.8
        solution0 = fahrenheit_to_celsius+273.15
    #kelvin to celsius
    elif userinput0 == 3 and userinput1 == 1:
        solution0 = userinput2-273.15
    #kelvin to fahrenheit
    elif userinput0 == 3 and userinput1 == 2:
        kelvin_to_celsius = userinput2-273.15
        solution0 = (kelvin_to_celsius*1.8)+32
    else:
        print("--------------------------")
        print("Input Error.")
        time.sleep(3)
        temperature_translator()

    #temperature menu
    print("--------------------------")
    print("Temperature Translator")
    print("--------------------------")

    #print solution
    print("{0} {1} to {2} = {3} {2}".format(userinput2, units[userinput0], units[userinput1], solution0))
    print("--------------------------")
    print("Please Wait For 5 Seconds...")
    print("--------------------------")
    time.sleep(5)

    #route back to menu
    routing()

#angle translation menu
def angle_translator():

    import time

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    #angle menu
    print("--------------------------")
    print("Angle Translator")
    print("--------------------------")

    #choose unit menu
    print("Press 1 For Degree To Radian")
    print("Press 2 For Radian To Degree")
    print("--------------------------")

    #get input
    try:
        userinput0 = int(input("Input: "))
        if userinput0 < 1 or userinput0 > 2:
            print("--------------------------")
            print("Input Error.")
            time.sleep(3)
            angle_translator()
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(3)
        angle_translator()

    #clear screen
    clear()

    #angle menu
    print("--------------------------")
    print("Angle Translator")
    print("--------------------------")

    #input number menu
    print("Input Number To Translate")
    print("--------------------------")

    #get input
    try:
        userinput1 = float(input("Input: "))
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(3)
        angle_translator()

    #list of units
    units = [0, "Radian", "Degree"]
    solution0 = 0
    unit_a = 0
    unit_b = 0

    #degree to radian
    if userinput0 == 1:
        solution0 = userinput1/57.2957795
        unit_a = 2
        unit_b = 1
    #radian to degree
    elif userinput0 == 2:
        solution0 = userinput1*57.2957795
        unit_a = 1
        unit_b = 2
    else:
        print("--------------------------")
        print("Unexpected Error.")
        time.sleep(2)
        angle_translator()

    #clear screen
    clear()
    
    #angle menu
    print("--------------------------")
    print("Angle Translator")
    print("--------------------------")

    #print solution
    print("{0} {1} to {2} = {3} {2}".format(userinput1, units[unit_a], units[unit_b], solution0, units[unit_b]))
    print("--------------------------")
    print("Please Wait For 5 Seconds...")
    print("--------------------------")
    time.sleep(5)

    #route back to menu
    routing()

#route back to menu
def routing(calculator_type = 0):

    import time
    time.sleep(3)

    if calculator_type == 0:
        #load calculator menu
        calculator_menu()
    elif calculator_type == 1:
        #load scientific calculator menu
        scientific_calculator_menu()
    else:
        print("Calculator Type Error.")
        time.sleep(3)
        calculator()

#print solution function
def print_solution(solution0, num1, num2, number_count, operator_index, calculator_type):

    import time

    #string operators
    operators0 = ["+","-","x","/","%", "^"]
    operators1 = ["^2"]
    operators2 = ["sqrt(", "log"]

    #check validity and print
    if number_count == 2:
        print(num1,operators0[operator_index],num2,"=",solution0)
    elif number_count == 1:
        print(str(num1)+str(operators1[operator_index])+" = "+str(solution0))
    elif number_count == 0:
        print(str(operators2[operator_index])+str(num1)+")"+" = "+str(solution0))
    elif number_count == -1:
        print(str(operators2[operator_index])+str(num2)+"("+str(num1)+")"+" = "+str(solution0))
    else:
        print("--------------------------")
        print("Unexpected Error.")
        time.sleep(2)
        calculator()
    print("--------------------------")
    routing(calculator_type)

#write your own function function
def write_your_function():

    import time

    from osx.systemx.additional_functions.clear import clear

    #clear screen
    clear()

    print("--------------------------")
    print("Write Your Own Function")
    print("Example: '(pow(2,3)+sqrt(144))/2+15")
    print("--------------------------")

    #get input
    try:
        userinput0 = str(input("Input: "))
        print("--------------------------")
        if type(eval(userinput0, globals())/1) == float:
            print("{0} = {1}".format(userinput0,eval(userinput0, globals())))
            time.sleep(3)
            calculator_menu()
        else:
            print("--------------------------")
            print("Input Error.")
            time.sleep(3)
            write_your_function()
    except:
        write_your_function()
    routing()

#addition function
def addition0(solution0):

    #addition menu
    print("--------------------------")
    print("Addition")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        num2 = int(input("Number 2: "))
        solution0 = num1 + num2
        print("--------------------------")
        number_count = 2
        operator_index = 0
        calculator_type = 0

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()

#subtraction function
def substraction0(solution0):

    #subtraction menu
    print("--------------------------")
    print("Substraction")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        num2 = int(input("Number 2: "))
        solution0 = num1 - num2
        print("--------------------------")
        number_count = 2
        operator_index = 1
        calculator_type = 0

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()

#multiplication function
def multiplication0(solution0):

    #multiplication menu
    print("--------------------------")
    print("Multiplication")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        num2 = int(input("Number 2: "))
        solution0 = num1 * num2
        print("--------------------------")
        number_count = 2
        operator_index = 2
        calculator_type = 0

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()

#division function
def division0(solution0):

    #division menu
    print("--------------------------")
    print("Division")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        num2 = int(input("Number 2: "))
        
        try:
            solution0 = num1 / num2
        #write infinity if divided by zero
        except ZeroDivisionError:
            solution0 = "infinity"
        
        print("--------------------------")
        number_count = 2
        operator_index = 3
        calculator_type = 0

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()

#square function
def square0(solution0):

    #square menu
    print("--------------------------")
    print("Square")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        solution0 = num1 * num1
        print("--------------------------")
        number_count = 1
        operator_index = 0
        num2 = 0
        calculator_type = 1

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()

#squareroot function
def squareroot0(solution0):

    #squareroot menu
    print("--------------------------")
    print("Square Root")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        solution0 = sqrt(num1)
        print("--------------------------")
        number_count = 0
        operator_index = 0
        num2 = 0
        calculator_type = 1

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()

#modulus function
def modulus0(solution0):

    #modulus menu
    print("--------------------------")
    print("Modulus")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        num2 = int(input("Number 2: "))
        solution0 = num1 % num2
        print("--------------------------")
        number_count = 2
        operator_index = 4
        calculator_type = 1

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()
        
#exponent function
def exponent0(solution0):

    #exponent menu
    print("--------------------------")
    print("Exponent")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        num2 = int(input("Number 2: "))
        solution0 = pow(num1, num2)
        print("--------------------------")
        number_count = 2
        operator_index = 5
        calculator_type = 1

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()
        
#logarithm function
def logarithm0(solution0):

    #logarithm menu
    print("--------------------------")
    print("Logarithm")
    print("--------------------------")

    #get input
    try:
        num1 = int(input("Number 1: "))
        num2 = int(input("Number 2: "))
        solution0 = log(num2, num1)
        print("--------------------------")
        number_count = -1
        operator_index = 1
        calculator_type = 1

        #return variables
        print_solution(solution0, num1, num2, number_count, operator_index, calculator_type)
    except:
        calculator()
