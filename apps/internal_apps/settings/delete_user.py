def delete_user():
    
    import os
    import time

    from start import start

    from osx.systemx.additional_functions.clear import clear

    from apps.menu import menu

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #login page
    print("--------------------------")
    time.sleep(0.2)
    print("Welcome, User")
    time.sleep(0.2)
    print("--------------------------")
    time.sleep(0.2)

    #get input
    try:
        username_input = str(input("Input Username: "))
        time.sleep(0.2)
        print("--------------------------")
        time.sleep(0.2)
        try:
            password_input = str(input("Input Password: "))
            time.sleep(0.2)
            print("--------------------------")
        except:
            print("Input Error.")
            time.sleep(2)
            delete_user()
    except:
        print("Input Error.")
        time.sleep(2)
        delete_user()

    #get credentials
    password_file = open(file_path+"/osx/user_management/user0/password.txt", "r")
    password_check = str(password_file.read())
    password_file.close()

    username_file = open(file_path+"/osx/user_management/user0/username.txt", "r")
    username_check = str(username_file.read())
    username_file.close()

    #check credentials
    if username_check == username_input:       
        if password_check == password_input:

            #clear screen
            clear()
            #are you sure page
            print("--------------------------")
            print("Are You Sure?")
            print("--------------------------")
            print("Press 1 For Yes")
            print("Press 0 For Main Menu")
            print("--------------------------")

            #get input
            try:
                username_sure = str(input("Input: "))
            except:
                print("Unexpected Error.")
                time.sleep(2)
                delete_user()

            if username_sure == "0":
                print("Returning To Main Menu")
                print("--------------------------")
                time.sleep(2)
                menu()
            else:
                pass

            #set credentials
            password_file = open(file_path+"/osx/user_management/user0/password.txt", "w")
            password_file.write("0")
            password_file.close()

            username_file = open(file_path+"/osx/user_management/user0/username.txt", "w")
            username_file.write("0")
            username_file.close()

            isRegistered_file = open(file_path+"/osx/user_management/user0/isRegistered.txt", "w")
            isRegistered_file.write("0")
            isRegistered_file.close()

            start()
        else:       
            print("Username Or Password Is Invalid.")
            time.sleep(3)
            delete_user()
    else:  
        print("Username Or Password Is Invalid.")
        time.sleep(3)
        delete_user()
