def clock():    

    #print
    date_type = 1
    print_clock(date_type)

#clock function
def print_clock(date_type):
    
    import time

    from datetime import datetime

    from osx.systemx.additional_functions.clear import clear

    #clear screen
    clear()

    #menu header
    print("--------------------------")
    print("Clock And Calendar")
    print("--------------------------")

    #print clock
    now = datetime.now()
    if date_type == 1:
        dt_string_date = now.strftime("%d/%m/%Y")
        dt_string_time = now.strftime("%H:%M:%S")
        print("Date:", dt_string_date)
        print("Time:", dt_string_time)
    elif date_type == 2:
        dt_string_time = now.strftime("%H:%M:%S")
        print("Time:", dt_string_time)
    elif date_type == 3:
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        print("Date Time:", dt_string)	
    elif date_type == 4:
        dt_string_date = now.strftime("%d/%m/%Y")
        print("Date:", dt_string_date)
    elif date_type == 5:
        dt_string_date = now.strftime("%m/%d/%Y")
        print("Date:", dt_string_date)
    elif date_type == 6:
        dt_string_date = now.strftime("%d/%m/%Y")
        dt_string_time = now.strftime("%H:%M:%S")
        print("Date:", dt_string_date)
        print("Time:", dt_string_time)
    elif date_type == 7:
        dt_string_date = now.strftime("%m/%d/%Y")
        dt_string_time = now.strftime("%H:%M:%S")
        print("Date:", dt_string_date)
        print("Time:", dt_string_time)
    else:
        print("Unexpected Error.")
        time.sleep(3)
        clock()
    
    #print menu
    clock_menu(date_type)

#menu function
def clock_menu(date_type):

    import time

    from apps.menu import menu

    #clock menu
    print("--------------------------")
    print("Press 1 For Date / Time")
    print("Press 2 For Time")
    print("Press 3 For Date Time")
    print("Press 4 For Date(Day, Month, Year)")
    print("Press 5 For Date(Month, Day, Year)")
    print("Press 6 For Date(Day, Month, Year) / Time")
    print("Press 7 For Date(Month, Day, Year) / Time")
    print("--------------------------")
    print("Press 0 For Main Menu")
    print("--------------------------")

    #default date type
    userinput0 = date_type

    #reload the clock every second if user is not pushing any key
    #else get input and reload again
    try:
        from osx.systemx.additional_functions.multitask import multitask
    
        #multitask function
        userdecide0 = multitask()

        #if key pressed get input
        if userdecide0 == 1:
            userinput0 = int(input("Input: "))
            #check input
            if userinput0 > 0 and userinput0 < 8:
                date_type = userinput0
                print_clock(date_type)
            elif userinput0 == 0:
                    menu()
            else:
                    clock()
        elif userdecide0 == 0:
            date_type = userinput0
            print_clock(date_type)
    except:
        clock() 
