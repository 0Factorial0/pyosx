def multitask():
    import sys
    import os

    try:
        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    test_os = os.name

    #clear screen
    clear()

    #check the os type, nt = windows, posix = unix
    #create a thread and wait for 1 second
    #then return if the key is pushed or not
    if test_os == "nt":

        import sys, time, msvcrt

        timeout = 0
        startTime = time.time()
        inp = None

        print("--------------------------") 
        print("Press Enter To Stop")
        print("--------------------------") 
        while True:
            if msvcrt.kbhit():
                inp = msvcrt.getch()
                break
            elif time.time() - startTime > timeout:
                break

        if inp:
            return 1
        else:
            return 0
    #create a thread and wait for 1 second
    #then return if the key is pushed or not
    elif test_os == "posix":

        from select import select

        print("--------------------------") 
        print("Press Enter To Stop")
        print("--------------------------") 
        timeout = 1
        rlist, wlist, xlist = select([sys.stdin], [], [], timeout)

        if rlist:
            return 1
        else:
            return 0
    else:
        return 1

def time_convert(sec):
    mins = sec // 60
    sec = sec % 60
    hours = mins // 60
    mins = mins % 60
    print("Elapsed Time = {0} hours {1} minutes {2} seconds".format(int(hours),int(mins),sec))
    print("-----------------------")

def stopwatch(x = 0, start_time = 0):

    import time

    try:
        from osx.systemx.additional_functions.clear import clear
        from apps.menu import menu
    except:
        print("Import Error.")

    if x == 0:
        #clear screen
        clear()

        #menu
        print("--------------------------") 
        print("Stopwatch")
        print("--------------------------") 
        print("Press 1 For Start")
        print("Press 0 For Menu")
        print("--------------------------") 

        #get input
        try:
            userinput0 = int(input("Input: "))
            if userinput0 < 0 or userinput0 > 1:
                print("--------------------------") 
                print("Input Error.")
                time.sleep(2)
                stopwatch()
        except:
            print("--------------------------") 
            print("Input Error.")
            time.sleep(2)
            stopwatch()

        #routing
        if userinput0 == 0:
            menu()
        elif userinput0 == 1:
            pass

        #starting time
        start_time = time.time()
    
    try:
        #multitask function
        userdecide0 = multitask()

        #if key pressed get input
        if userdecide0 == 1:
            #print time
            now_time = time.time()
            time_convert(now_time-start_time)
            print("Please Wait For 3 Seconds...")
            print("-----------------------")
            time.sleep(3)
            stopwatch()
        elif userdecide0 == 0:
            #print time
            now_time = time.time()
            time_convert(now_time-start_time)
            time.sleep(0.01)
            stopwatch(1, start_time)
    except:
        #print time
        now_time = time.time()
        time_convert(now_time-start_time)
        time.sleep(0.01)
        stopwatch(1, start_time)
