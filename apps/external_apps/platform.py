def platform():
    
    import os
    import time
    import pygame
    import sys
    import random

    try:
        from apps.menu import menu
        from osx.systemx.additional_functions.clear import clear

        from apps.external_apps.platform_resources.script.walk import walk
    except:
        print("Import Error.")

    #initialize the screen
    pygame.init()
    screen = pygame.display.set_mode((640,480))

    #set the title
    pygame.display.set_caption("Game")

    #framerate variable
    clock = pygame.time.Clock()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #test font
    test_font = pygame.font.Font(file_path+'/apps/external_apps/platform_resources/font/Pixeltype.ttf', 80)
    test_text = test_font.render('TEST', False, 'Purple')

    #load sprites
    background = pygame.image.load(file_path+'/apps/external_apps/platform_resources/background/background_evening.png')

    character_idle = pygame.image.load(file_path+'/apps/external_apps/platform_resources/character/stickman.png')

    '''
    character_walk = []
    for i in range(1,5):
        character_walk_frame = pygame.image.load(file_path+'/apps/external_apps/platform_resources/character/walk/frame'+i+".png")
        character_walk.append(character_walk_frame)
    '''

    slime = pygame.image.load(file_path+'/apps/external_apps/platform_resources/mob/slime/slime.png')
    slime1 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/mob/slime/slime_idle/1.png')
    slime2 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/mob/slime/slime_idle/2.png')
    slime3 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/mob/slime/slime_idle/3.png')
    slime4 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/mob/slime/slime_idle/4.png')
    slime5 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/mob/slime/slime_idle/5.png')
    slime6 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/mob/slime/slime_idle/6.png')

    brick = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/brick.png')
    
    grass = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass.png')
    grass2 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass2.png')
    grass3 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass3.png')
    grass4 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass4.png')
    grass5 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass5.png')
    grass6 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass6.png')
    grass7 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass7.png')
    grass8 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass8.png')

    grass_carpet = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/grass_carpet.png')

    dirt = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/dirt.png')

    lava = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/lava.png')
    lava2 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/lava2.png')

    water = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/water.png')
    water2 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/water2.png')

    cloud = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/cloud.png')
    cloud2 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/cloud2.png')
    cloud3 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/cloud3.png')

    coin = pygame.image.load(file_path+'/apps/external_apps/platform_resources/item/coin.png')

    door = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/tall/door.png')
    door2 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/tall/door2.png')
    double_door = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/tall/double_door.png')
    double_door2 = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/tall/double_door2.png')

    vine = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/vine.png')
    tall_vine = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/vine_tall.png')
    
    moss = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/moss.png')
    moss_vine = pygame.image.load(file_path+'/apps/external_apps/platform_resources/tile/viney_moss.png')

    hopeful = pygame.mixer.music.load(file_path+'/apps/external_apps/platform_resources/music/hopeful.wav')
    pygame.mixer.music.play(-1)

    player_x = 160
    player_y = 352

    slime_counter = 1

    jumping = False

    gravity = 1
    jump_height = 10
    velocity = jump_height

    #clear screen
    clear()

    xlist0 = []
    for i in range(15):
        for j in range(16):
            xlist0.append(random.randint(0,1))

    while True:
        #get events
        for event in pygame.event.get():
            #if event is quit, close the game
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
                menu()
        
        '''
        #random movement test
        a = random.randint(0,1)
        b = random.randint(0,1)
        if a == 0:
            player_x += 4
        elif a == 1:
            player_x -= 4
        if b == 0:
            jumping = True
        '''
        
        #moving x axis
        keys = pygame.key.get_pressed()
        moving = ""
        if keys[pygame.K_LEFT]:
            moving = "left"
        elif keys[pygame.K_RIGHT]:
            moving = "right"
        player_x = walk(player_x, moving)

        #jumping
        if keys[pygame.K_UP]:
            jumping = True
        if jumping:
            player_y -= velocity
            velocity -= gravity
            if velocity < -jump_height:
                jumping = False
                velocity = jump_height

        #connect the screen in x axis
        if player_x > 640:
            player_x = -32
        elif player_x < -32:
            player_x = 640

        #draw background
        screen.blit(background, (0,0))
        
        #TEST SCREEN
        #draw clouds
        screen.blit(cloud, (50,50))
        screen.blit(cloud2, (500,100))
        screen.blit(cloud3, (300,150))

        #draw first line of ground
        x = 0
        counter = 0
        for i in range(20):
            if counter < 5:
                screen.blit(grass, (x, 384))
            elif counter == 5:
                screen.blit(grass2, (x, 384))
            elif counter > 5 and counter < 10:
                screen.blit(water, (x, 384))
            elif counter == 10:
                screen.blit(grass3, (x, 384))
            elif counter == 11:
                screen.blit(grass, (x, 384))
            elif counter == 12:
                screen.blit(grass2, (x, 384))
            elif counter > 12 and counter < 15:
                screen.blit(lava, (x, 384))
            elif counter == 15:
                screen.blit(grass3, (x, 384))
            elif counter > 15 and counter < 20:
                screen.blit(grass, (x, 384))
            counter += 1
            x += 32

        #draw second line of ground
        x = 0
        counter = 0
        for i in range(20):
            if counter < 5:
                screen.blit(dirt, (x,416))
            elif counter == 5:
                screen.blit(grass4, (x,416))
            elif counter > 5 and counter < 10:
                screen.blit(water2, (x,416))
            elif counter == 10:
                screen.blit(grass5, (x,416))
            elif counter == 11:
                screen.blit(dirt, (x,416))
            elif counter == 12:
                screen.blit(grass4, (x,416))
            elif counter > 12 and counter < 15:
                screen.blit(lava2, (x,416))
            elif counter == 15:
                screen.blit(grass5, (x,416))
            elif counter > 15 and counter < 20:
                screen.blit(dirt, (x,416))
            counter += 1
            x += 32

        #draw third line of ground
        x = 0
        counter = 0
        for i in range(20):
            if counter < 5:
                screen.blit(dirt, (x,448))
            elif counter == 5:
                screen.blit(grass8, (x,448))
            elif counter > 5 and counter < 10:
                screen.blit(grass6, (x,448))
            elif counter == 10:
                screen.blit(grass7, (x,448))
            elif counter > 10 and counter < 12:
                screen.blit(dirt, (x,448))
            elif counter == 12:
                screen.blit(grass8, (x,448))
            elif counter > 12 and counter < 15:
                screen.blit(grass6, (x,448))
            elif counter == 15:
                screen.blit(grass7, (x,448))
            elif counter > 15 and counter < 20:
                screen.blit(dirt, (x,448))
            counter += 1
            x += 32

        #draw doors
        screen.blit(door, (0,320))
        screen.blit(door2, (352,320))
        screen.blit(double_door, (224, 32))
        screen.blit(double_door2, (576, 128))

        #draw bricks
        screen.blit(brick, (128, 320))
        x = 192
        for i in range(3):
            screen.blit(brick, (x,256))
            x += 32
        x = 192
        for i in range(4):
            screen.blit(brick, (x,96))
            x += 32
        x = 384
        for i in range(3):
            screen.blit(brick, (x,192))
            x += 32
        x = 544
        for i in range(3):
            screen.blit(brick, (x,192))
            x += 32
        x = 32
        for i in range(3):
            screen.blit(brick, (x,160))
            x += 32

        #draw coins
        screen.blit(coin, (224,224))
        x = 480
        for i in range(5):
            screen.blit(coin, (x,320))
            x += 32
        x = 384
        for i in range(3):
            screen.blit(coin, (x,160))
            x += 32
        x = 32
        for i in range(3):
            screen.blit(coin, (x,128))
            x += 32

        #draw mob
        if slime_counter == 35:
            slime_counter = 5
        
        if slime_counter >= 5 and slime_counter < 10:
            screen.blit(slime1, (544, 352))
            screen.blit(slime1, (544, 160))
            screen.blit(slime1, (player_x, player_y))
        elif slime_counter >= 10 and slime_counter < 15:
            screen.blit(slime2, (544, 352))
            screen.blit(slime2, (544, 160))
            screen.blit(slime2, (player_x, player_y))
        elif slime_counter >= 15 and slime_counter < 20:
            screen.blit(slime3, (544, 352))
            screen.blit(slime3, (544, 160))
            screen.blit(slime3, (player_x, player_y))
        elif slime_counter >= 20 and slime_counter < 25:
            screen.blit(slime4, (544, 352))
            screen.blit(slime4, (544, 160))
            screen.blit(slime4, (player_x, player_y))
        elif slime_counter >= 25 and slime_counter < 30:
            screen.blit(slime5, (544, 352))
            screen.blit(slime5, (544, 160))
            screen.blit(slime5, (player_x, player_y))
        elif slime_counter >= 30 and slime_counter < 35:
            screen.blit(slime6, (544, 352))
            screen.blit(slime6, (544, 160))
            screen.blit(slime6, (player_x, player_y))
        slime_counter += 1

        #draw carpet
        x = 0
        counter = 0
        for i in range(20):
            if counter < 5:
                screen.blit(grass_carpet, (x, 352))
            elif counter == 5:
                screen.blit(grass_carpet, (x, 352))
            elif counter > 5 and counter < 10:
                pass
            elif counter == 10:
                screen.blit(grass_carpet, (x, 352))
            elif counter == 11:
                screen.blit(grass_carpet, (x, 352))
            elif counter == 12:
                screen.blit(grass_carpet, (x, 352))
            elif counter > 12 and counter < 15:
                pass
            elif counter == 15:
                screen.blit(grass_carpet, (x, 352))
            elif counter > 15 and counter < 20:
                screen.blit(grass_carpet, (x, 352))
            counter += 1
            x += 32

        #draw character
        screen.blit(character_idle, (48, 320))

        #draw test text
        screen.blit(test_text, (500,50))
        '''
        
        #Vine Test
        #draw vine
        counter = 0
        for i in range(0,15):
            for j in range(0,20):
                if i == 14:
                    screen.blit(grass, (j*32,i*32))
                elif i == 13 or i == 12:
                    pass
                elif i == 11:
                    screen.blit(tall_vine, (j*32,i*32))
                else:
                    if xlist0[counter] == 0:
                        screen.blit(moss, (j*32,i*32))
                    elif xlist0[counter] == 1:
                        screen.blit(moss_vine, (j*32,i*32))
                    counter += 1

        #draw slime character
        if slime_counter == 35:
            slime_counter = 5
        
        if slime_counter >= 5 and slime_counter < 10:
            screen.blit(slime1, (player_x, player_y))
        elif slime_counter >= 10 and slime_counter < 15:
            screen.blit(slime2, (player_x, player_y))
        elif slime_counter >= 15 and slime_counter < 20:
            screen.blit(slime3, (player_x, player_y))
        elif slime_counter >= 20 and slime_counter < 25:
            screen.blit(slime4, (player_x, player_y))
        elif slime_counter >= 25 and slime_counter < 30:
            screen.blit(slime5, (player_x, player_y))
        elif slime_counter >= 30 and slime_counter < 35:
            screen.blit(slime6, (player_x, player_y))
        slime_counter += 1

        '''

        #draw the screen
        #update all elements
        pygame.display.update()

        #max framerate
        clock.tick(60)
