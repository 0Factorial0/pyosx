def character_building():

    import time
    import os
    
    from osx.systemx.additional_functions.delay_print import delay_print
    from osx.systemx.additional_functions.clear import clear

    from apps.external_apps.wish_you_not.resources.menu.load_game import load_game

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #get the files
    username_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/username.txt", "w")
    password_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/password.txt", "w")
    
    isRegistered_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/isRegistered.txt", "w")

    name_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/settings/name.txt", "w")
    age_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/settings/age.txt", "w")
    gender_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/settings/gender.txt", "w")
    race_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/settings/race.txt", "w")
    class_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/settings/class.txt", "w")
    sub_class_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/settings/sub_class.txt", "w")

    #get player input
    print("--------------------------")
    story1_1 = "Bu şanslılardan biri olan: "
    delay_print(story1_1, 0)

    #get name
    try:
        player_name = input("")
        username_file.write(player_name)
        name_file.write(player_name)

        username_file.close()
        name_file.close()
    except:
        print("Input Error.")
        time.sleep(2)
        character_building()

    time.sleep(0.3)
    print("")
    story1_1 = "Haerndean'a yeni gelmiş, yaşı(18-70): "
    delay_print(story1_1, 0)

    #get age
    try:
        player_age = int(input(""))
        if player_age < 18 or player_age > 70:
            print("")
            print("Input Error.")
            time.sleep(2)
            character_building()
        else:
            if player_age > 17 and player_age < 30:
                age_file.write("young")
            elif player_age > 29 and player_age < 50:
                age_file.write("middle age")
            elif player_age > 49 and player_age < 71:
                age_file.write("old")
            else:
                print("")
                print("Input Error.")
                time.sleep(2)
                character_building()
            age_file.close()
    except:
        print("")
        print("Input Error.")
        time.sleep(2)
        character_building()

    time.sleep(0.3)
    story1_1 = "olan bir(erkek/kadın): "
    delay_print(story1_1, 0)
    
    #get gender
    try:
        player_gender = str(input(""))
        if player_gender == "erkek":
            gender_file.write("male")
        elif player_gender == "kadın":
            gender_file.write("female")
        else:
            print("")
            print("Input Error.")
            time.sleep(2)
            character_building()
        gender_file.close()
    except:
        print("")
        print("Input Error.")
        time.sleep(2)
        character_building()
        
    time.sleep(0.3)
    story1_1 = "kişiydi. Mesleğini(doktor, mühendis, sanatçı): "
    delay_print(story1_1, 0)
    
    #get sub class
    try:
        player_sub_class = str(input(""))
        if player_sub_class == "doktor":
            sub_class_file.write("doctor")
        elif player_sub_class == "mühendis":
            sub_class_file.write("engineer")
        elif player_sub_class == "sanatçı":
            sub_class_file.write("artist")
        else:
            print("")
            print("Input Error.")
            time.sleep(2)
            character_building()
        sub_class_file.close()
    except:
        print("")
        print("Input Error.")
        time.sleep(2)
        character_building()
        
    time.sleep(0.3)
    story1_1 = "olarak yapmaktaydı, bu yüzden seçilmişti ve"
    delay_print(story1_1, 1)
        
    story1_1 = "Sceek'a Vipa'nın şanslı vatandaşlarından biri olmuştu."
    delay_print(story1_1, 1)
        
    time.sleep(2)
    print("")
    story1_1 = "Eğer bir gün savaşması gerekirse nasıl dövüşeceğini düşünmüştü."
    delay_print(story1_1, 1)
    print("")
    story1_1 = "Buna göre planı(nişancı, kavgacı, dengeli): "
    delay_print(story1_1, 0)
    
    #get class
    try:
        player_class = str(input(""))
        if player_class == "nişancı":
            class_file.write("marksman")
        elif player_class == "kavgacı":
            class_file.write("fighter")
        elif player_class == "dengeli":
            class_file.write("ranger")
        else:
            print("")
            print("Input Error.")
            time.sleep(2)
            character_building()
        class_file.close()
    except:
        print("")
        print("Input Error.")
        time.sleep(2)
        character_building()

    story1_1 = "bir tarz takınmak olmuştu."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        character_building()
    
    race_file.write("human")
    race_file.close()

    #clear screen
    clear()

    #get password
    print("--------------------------")
    story1_1 = "Ve bir gün en çok istediği şey olan: "
    delay_print(story1_1, 0)
    
    #get password
    try:
        player_password = str(input(""))
        password_file.write(player_password)
    except:
        print("")
        print("Input Error.")
        time.sleep(2)
        character_building()

    story1_1 = "denen şeye ulaşabileceği umuduyla hayaller aleminde"
    delay_print(story1_1, 1)
    story1_1 = "gezmekteyken, dışarıdan gelen atlı sesiyle yatağından düştü ve uyandı."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        isRegistered_file.write("1")
        isRegistered_file.close()
        load_game()
    except:
        print("Press Enter Error.")
        character_building()
