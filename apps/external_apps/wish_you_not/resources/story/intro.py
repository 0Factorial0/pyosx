def intro():

    import time
    
    from osx.systemx.additional_functions.delay_print import delay_print
    from osx.systemx.additional_functions.clear import clear

    from apps.external_apps.wish_you_not.resources.story.character_building import character_building

    #clear screen
    clear()

    #story intro
    print("--------------------------")
    story1_1 = "Herkes Nerede?"
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")

    story1_1 = "Her şey 1950 yılında başladı. Fizikçi Enrico Fermi; kendisi gibi fizikçi"
    delay_print(story1_1, 1)
    story1_1 = "olan arkadaşlarıyla bir öğle yemeğinde oturmuş, insanlık ve uzay ile ilgili düşünmekteydi."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")

    story1_1 = "Fermi, \"Eğer Samanyolu dahilinde yüksek sayıda ileri Dünya Dışı Uygarlık mevcutsa,"
    delay_print(story1_1, 1)
    story1_1 = "neden onlara ait uzay araçları ya da sondalar gibi kanıtlara rastlamıyoruz?\" sorusunu sorarak"
    delay_print(story1_1, 1)
    story1_1 = "bir soru işareti selinin başlangıcına imzasını atmış oldu..."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        intro()

    print("--------------------------")
    story1_1 = "Çok büyük bir hata yaptık."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Geri dönülemez bir hata..."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        intro()

    print("--------------------------")
    story1_1 = "70'li yıllarda Dünya dışına olan ilginin iyice artmasıyla birlikte Arecibo Mesajı isimli"
    delay_print(story1_1, 1)
    story1_1 = "bir vektör 16 Kasım 1974 yılında uzaya gönderildi. İçinde insanlığın çok değerli bilgileri vardı."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Hemen ardından Voyager isimli program başladı. Çeşitli fotoğraflar çekecek ve saatte"
    delay_print(story1_1, 1)
    story1_1 = "61,500km/h hız ile sınırsızca Dünya'dan uzaklaşacak bir makineydi bu..."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        intro()

    print("--------------------------")
    story1_1 = "Altın Plaklar olarak bilinen, insanlar hakkında her bilgiyi içeren bir kayıt."
    delay_print(story1_1, 1)
    time.sleep(1)
    print("")
    story1_1 = "İntihar notumuz."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Bu kayıtlarda Güneş Sistemindeki ve Evrendeki yerimiz, DNA yapımız, konuştuğumuz"
    delay_print(story1_1, 1)
    story1_1 = "diller ve her türlü davranışımız açık şekilde ifade edilmişti."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Sanki: \"İşte bizi böyle yok edebilirsiniz!\" diye bağırıyorduk..."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        intro()

    print("--------------------------")
    story1_1 = "Karanlık bir uzayın ortasında, sonuçlarını bilmeden, zavallı bir şekilde etrafa bilgi saçıyorduk."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Hatamızı fark ettiğimizde artık çok geçti. Mesajı yakalayıp yok etmek"
    delay_print(story1_1, 1)
    story1_1 = "için gidilmesi gereken onca ışık yılını hızlıca aşabilecek kadar gelişmemiştik."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Vahşi bir ırkın bu mesajlara denk gelmesi an meselesiydi..."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Neyse ki şanslıydık! Ve bizi yok etmek yerine süründürecek bir bakıcıya denk geldik..."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        intro()

    print("--------------------------")
    story1_1 = "Griler. Samanyolu Galaksisi'ndeki Galaktik İmparatorluğun, Simbiyot denen"
    delay_print(story1_1, 1)
    story1_1 = "önüne geleni sömüren varlığın son ürünü olan keşifçi yaratıklar."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Griler genelde yeni Gezegenler ve Türler keşfetmekle görevlidir."
    delay_print(story1_1, 1)
    time.sleep(2)
    story1_1 = "Gittikleri yerde yeni bir düzen olması için onlara teknoloji vaadi verir,"
    delay_print(story1_1, 1)
    story1_1 = "hiçbir kaynakları kalmayana kadar sömürür ve kendi çıkarları için kullanır."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Emirlerini doğrudan Kırmızılar denen ırktan alır ve uygular."
    delay_print(story1_1, 1)
    time.sleep(2)
    story1_1 = "Hisleri ya da düşünceleri yoktur, sadece yapacağı işi"
    delay_print(story1_1, 1)
    time.sleep(2)
    story1_1 = "en iyi şekilde yapması için üretilmiştir..."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        intro()

    print("--------------------------")
    story1_1 = "Kocaman gözleri ve uzun parmaklarıyla dünyamıza iniş yaptılar. Teknolojileri o kadar kuvvetliydi"
    delay_print(story1_1, 1)
    story1_1 = "ki hiç kimse onları durdurmak için önlerine çıkmaya cesaret edemedi."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Yok edilecektik ya da anlaşma yapacaktık."
    delay_print(story1_1, 1)
    time.sleep(2)
    story1_1 = "Anlaşmak zorunda kaldık."
    time.sleep(2)
    print("")
    delay_print(story1_1, 1)
    story1_1 = "Kaynaklarımız ve iş gücümüz karşılığında bize yüksek teknoloji verdiler."
    delay_print(story1_1, 1)
    story1_1 = "Bizi eğittiler ve Mars'da koloni kurmamızı sağladılar."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Artık daha gelişmiş bir insanlık vardı ama insanlıktan eser kalmamıştı..."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        intro()

    print("--------------------------")
    story1_1 = "Gezegenimizden çeşitli madenler, topraklar ve su kaynaklarını alarak başka"
    delay_print(story1_1, 1)
    story1_1 = "yerlere götürdüler ve bizi minimum nüfusa inmeye zorladılar."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Artık dünyada sadece 20 bin insan vardı ve bunların çoğu"
    delay_print(story1_1, 1)
    story1_1 = "çölde yaşayan yazılımcılardı."
    delay_print(story1_1, 1)
    time.sleep(2)
    print("")
    story1_1 = "Bazı şanslı mühendisler, sanatçılar, yöneticiler ve doktorlar su "
    delay_print(story1_1, 1)
    story1_1 = "kaynaklarının yakınında yaşayabiliyordu..."
    delay_print(story1_1, 1)
    print("--------------------------")

    try:
        input("Enter'a Basınız")
        print("--------------------------")
        clear()
    except:
        print("Press Enter Error.")
        intro()

    character_building()