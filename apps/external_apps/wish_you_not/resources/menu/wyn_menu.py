def wyn_menu():
    
    import os
    import time

    from apps.menu import menu

    from osx.systemx.additional_functions.clear import clear

    from apps.external_apps.wish_you_not.resources.menu.load_game import load_game
    from apps.external_apps.wish_you_not.resources.menu.new_game import new_game
    from apps.external_apps.wish_you_not.resources.menu.settings_menu import settings_menu

    from apps.external_apps.wish_you_not.resources.story.character_building import character_building

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #check if registered
    isRegistered_file = open(file_path+"/apps/external_apps/wish_you_not/user/user0/isRegistered.txt", "r")
    isRegistered = isRegistered_file.read()

    isRegistered_file.close()

    #main menu
    print("--------------------------")
    print("Wish You Not")
    print("--------------------------")
    if isRegistered == "1":
        print("Press 1 For Load Game")
    else:
        pass
    print("Press 2 For New Game")
    print("Press 3 For Settings")
    print("Press 0 For Quit")
    print("--------------------------") 

    #get input
    try:
        userinput0 = str(input("Input: "))
    except:
        print("Input Error.")
        time.sleep(2)
        wyn_menu()

    #check input and route
    if userinput0 == "1" and isRegistered == "1":
        load_game()
    elif userinput0 == "2":
        new_game()
    elif userinput0 == "3":
        settings_menu()
    elif userinput0 == "23":
        character_building()
    elif userinput0 == "0":
        menu()
    else:
        print("Wrong Input.")
        time.sleep(2)
        wyn_menu()
