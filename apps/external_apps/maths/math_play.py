def math_play():
    import time
    import os

    #import modules
    try:
        from apps.menu import menu
        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    #clear screen
    clear()

    #main menu
    print("--------------------------")
    print("Welcome, {0}".format(getUsername()))
    print("Current Points: {0}".format(getPoints()))
    print("--------------------------")
    print("Press 1 For Easy Mode")
    print("Press 2 For Medium Mode")
    print("Press 3 For Hard Mode")
    print("--------------------------")
    print("Press 0 For Main Menu")
    print("--------------------------")

    #get input
    try:
        game_type = int(input("Input: "))
        if game_type < 1 or game_type > 3:
            print("--------------------------")
            print("Input Error.")
            time.sleep(2)
            math_play()
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(2)
        math_play()

    #clear screen
    clear()

    #route
    if game_type == 0:
        menu()
    elif game_type == 1:
        math_game_easy_mode(getPoints())
    elif game_type == 2:
        math_game_medium_mode(getPoints())
    elif game_type == 3:
        math_game_hard_mode(getPoints())
    else:
        print("Unexpected Error.")
        time.sleep(2)
        math_play()

def getUsername():
    import os

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))
    
    #get username
    username_file = open(file_path+"/apps/external_apps/maths/user/user0/username.txt", "r")
    username = username_file.read()
    username_file.close()

    return username

def getPoints():
    import os

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))
    
    #get points
    points_file = open(file_path+"/apps/external_apps/maths/user/user0/points.txt", "r")
    points = points_file.read()
    points_file.close()

    return int(points)

def setPoints(points):
    import os

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))
    
    #set points
    points_file = open(file_path+"/apps/external_apps/maths/user/user0/points.txt", "w")
    points_file.write(str(points))
    points_file.close()

def math_game_easy_mode(points):
    import random
    import time

    #calculate the game
    x = random.randint(1,10)
    y = random.randint(1,10)
    operator_list = ["+","-","*"]
    operator = operator_list[random.randint(0,2)]
    solution = eval("{0}{1}{2}".format(x,operator,y))

    #easy mode menu
    print("--------------------------")
    print("Easy Mode")
    print("Current Points: {0}".format(points))
    print("--------------------------")
    print("{0} {1} {2} = ".format(x,operator,y), end="")

    #get input
    try:
        userinput0 = int(input(""))
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(2)
        math_play()
    
    #check if user was right or not
    if solution == userinput0:
        print("--------------------------")
        print("Congratulations You Answered Correctly!")
        print("{0} Earned 1 Points".format(getUsername()))
        points += 1
    else:
        print("--------------------------")
        print("You Answered Wrong.")
        print("{0} Lost 1 Points".format(getUsername()))
        points -= 1

    #please wait for 3 seconds
    print("--------------------------")
    print("Please Wait For 3 Seconds")
    print("--------------------------")
    time.sleep(3)
    setPoints(points)

    #route back to menu
    math_play()

def math_game_medium_mode(points):
    math_play()

def math_game_hard_mode(points):
    math_play()