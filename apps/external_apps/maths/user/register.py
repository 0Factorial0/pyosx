def register():

    import os
    import time

    from apps.menu import menu
    
    from apps.external_apps.math_game import math_game
    from apps.external_apps.maths.math_play import math_play

    from osx.systemx.additional_functions.clear import clear

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #register page
    print("--------------------------")
    time.sleep(0.2)
    print("Welcome, New Math Game User.")
    time.sleep(0.2)
    print("You Need To Register Before Starting Up The Game.")
    time.sleep(0.2)
    print("--------------------------")
    time.sleep(0.2)

    #get input
    try:
        username_input = str(input("Input Username: "))
        time.sleep(0.2)
        print("--------------------------")
        time.sleep(0.2)
        try:
            password_input = str(input("Input Password: "))
            time.sleep(0.2)
            print("--------------------------")
        except:
            print("Input Error.")
            time.sleep(2)
            math_game()
    except:
        print("Input Error.")
        time.sleep(2)
        math_game()

    #set credentials
    password_file = open(file_path+"/apps/external_apps/maths/user/user0/password.txt", "w")
    password_file.write(password_input)
    password_file.close()

    username_file = open(file_path+"/apps/external_apps/maths/user/user0/username.txt", "w")
    username_file.write(username_input)
    username_file.close()

    isRegistered_file = open(file_path+"/apps/external_apps/maths/user/user0/isRegistered.txt", "w")
    isRegistered_file.write("1")
    isRegistered_file.close()

    isRegistered_file = open(file_path+"/apps/external_apps/maths/user/user0/points.txt", "w")
    isRegistered_file.write("0")
    isRegistered_file.close()

    #go to menu page
    math_play()