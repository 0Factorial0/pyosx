def delete_save():

    import os

    from apps.external_apps.math_game import math_game

    from osx.systemx.additional_functions.clear import clear
    
    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #set credentials
    password_file = open(file_path+"/apps/external_apps/maths/user/user0/password.txt", "w")
    password_file.write("")
    password_file.close()

    username_file = open(file_path+"/apps/external_apps/maths/user/user0/username.txt", "w")
    username_file.write("")
    username_file.close()

    isRegistered_file = open(file_path+"/apps/external_apps/maths/user/user0/isRegistered.txt", "w")
    isRegistered_file.write("0")
    isRegistered_file.close()

    isRegistered_file = open(file_path+"/apps/external_apps/maths/user/user0/points.txt", "w")
    isRegistered_file.write("0")
    isRegistered_file.close()

    math_game()