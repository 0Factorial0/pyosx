def login():

    import os
    import time

    from apps.external_apps.math_game import math_game
    from apps.external_apps.maths.math_play import math_play

    from apps.external_apps.maths.user.delete_save import delete_save

    from osx.systemx.additional_functions.clear import clear

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #get credentials
    password_file = open(file_path+"/apps/external_apps/maths/user/user0/password.txt", "r")
    password_check = str(password_file.read())
    password_file.close()

    username_file = open(file_path+"/apps/external_apps/maths/user/user0/username.txt", "r")
    username_check = str(username_file.read())
    username_file.close()

    #login page
    print("--------------------------")
    time.sleep(0.2)
    print("Welcome, {0}".format(username_check))
    print("Input 0 To Delete Existing Save")
    time.sleep(0.2)
    print("--------------------------")
    time.sleep(0.2)

    #get input
    try:
        username_input = str(input("Input Username: "))
        if username_input == "0":
            delete_save()
        time.sleep(0.2)
        print("--------------------------")
        time.sleep(0.2)
        try:
            password_input = str(input("Input Password: "))
            time.sleep(0.2)
            print("--------------------------")
        except:
            print("Input Error.")
            time.sleep(2)
            login()
    except:
        print("Input Error.")
        time.sleep(2)
        login()

    #check credentials
    if username_check == username_input and password_check == password_input:       
        math_play()
    else:  
        print("Username Or Password Is Invalid.")
        time.sleep(3)
        math_game()
