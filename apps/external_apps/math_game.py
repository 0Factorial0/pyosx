def math_game():

    import os
    import time
        
    try:
        from apps.external_apps.maths.user.login import login
        from apps.external_apps.maths.user.register import register

        from apps.menu import menu

        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #check if registered
    isRegistered_file = open(file_path+"/apps/external_apps/maths/user/user0/isRegistered.txt", "r")
    isRegistered = isRegistered_file.read()

    isRegistered_file.close()

    #main menu / routing
    print("--------------------------")
    print("Continue To Math Game?")
    print("--------------------------")
    print("Press 1 For YES")
    print("Press 0 For NO")
    print("--------------------------")

    #get input
    try:
        program_type = str(input("Input: "))
    except:
        print("Unexpected Error.")
        time.sleep(2)
        menu()

    #check input
    if program_type == "0":
        menu()
    elif program_type == "1":
        #if user is registered go to login page, or not go to register page
        if isRegistered == "1":
            login()
        elif isRegistered == "0":
            register()
        else:
            print("--------------------------")
            print("User Management Error.")
            time.sleep(2)
            math_game()
    else:
        print("--------------------------")
        print("Input Error.")
        time.sleep(2)
        menu()

