def sorting():

    import time
    import ast
        
    #import modules
    try:
        from apps.menu import menu

        from apps.external_apps.sorting_algorithms.selection import selection
        from apps.external_apps.sorting_algorithms.bubble import bubble
        from apps.external_apps.sorting_algorithms.radix import radix

        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    #clear screen
    clear()

    #print algorithm menu
    print("--------------------------")
    print("Select Sorting Algorithm")
    print("--------------------------")
    print("Press 1 For Selection Sort")
    print("Press 2 For Bubble Sort")
    print("Press 3 For Radix Sort")
    print("--------------------------")
    print("Press 0 For Main Menu")
    print("--------------------------")
    
    #get input
    try:
        userinput0 = int(input("Input: "))
        if userinput0 < 0 or userinput0 > 3:
            print("--------------------------")
            print("Input Error.")
            time.sleep(2)
            sorting()
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(2)
        sorting()

    if userinput0 == 0:
        menu()

    #clear screen
    clear()

    #print list type menu
    print("--------------------------")
    print("Select List Type")
    print("--------------------------")
    print("Press 1 For Auto Generated")
    print("Press 2 For My Input")
    print("--------------------------")
    
    #get input
    try:
        userinput1 = int(input("Input: "))
        if userinput1 < 1 or userinput1 > 2:
            print("--------------------------")
            print("Input Error.")
            time.sleep(2)
            sorting()
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(2)
        sorting()

    #clear screen
    clear()

    #define empty user list
    userlist = []

    #get list from user
    if userinput1 == 2:
        
        #print input list menu
        print("--------------------------")
        print("Input Unsorted List")
        print("Syntax: 12,5,82,2,6")
        print("--------------------------")

        #get input
        try:
            userinput2 = str(input("Input: "))
            userlist = ast.literal_eval("[{0}]".format(userinput2))
            if type(userlist) != list:
                print("--------------------------")
                print("Syntax Error.")
                time.sleep(2)
                sorting()
            elif userlist == []:
                print("--------------------------")
                print("Empty List Error.")
                time.sleep(2)
                sorting()
        except TypeError:
            print("--------------------------")
            print("Input Error.")
            time.sleep(2)
            sorting()
        except SyntaxError:
            print("--------------------------")
            print("Syntax Error.")
            time.sleep(2)
            sorting()
        except:
            print("--------------------------")
            print("Unexpected Error.")
            time.sleep(2)
            sorting()
            
    #routing
    if userinput0 == 1:
        selection(userlist)
    elif userinput0 == 2:
        bubble(userlist)
    elif userinput0 == 3:
        radix(userlist)
    else:
        menu()

    #please wait for 5 seconds
    print("--------------------------")
    print("Please Wait For 5 Seconds...")
    print("--------------------------")

    #route back
    time.sleep(5)
    sorting()