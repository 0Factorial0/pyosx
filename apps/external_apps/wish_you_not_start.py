def wish_you_not_start():
    
    import os
    import time

    try:
        from apps.menu import menu

        from osx.systemx.additional_functions.clear import clear

        from apps.external_apps.wish_you_not.resources.menu.wyn_menu import wyn_menu
    except:
        print("Import Error.")

    clear()

    wyn_menu()