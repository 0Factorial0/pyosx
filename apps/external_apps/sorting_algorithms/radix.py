def counting_sort(list0, place):

    import time

    #import menu
    try:
        from apps.menu import menu
    except:
        print("Import Error.")

    size = len(list0)
    output = [0] * size
    count = [0] * 10

    #calculate count of numbers
    for i in range(0, size):
        index = list0[i] // place
        try:
            count[index % 10] += 1
        except:
            print("--------------------------")
            print("Float Error.")
            time.sleep(5)
            menu()

    #calculate cumulative content
    for i in range(1, 10):
        count[i] += count[i-1]

    #place the numbers in sorted order
    i = size - 1
    while i >= 0:
        index = list0[i] // place
        output[count[index % 10]-1] = list0[i]
        count[index % 10] -= 1
        i -= 1

    for i in range(0, size):
        list0[i] = output[i]

def bucket_sort():

    #TO-DO
    '''
    -Add bucket Sort As An Option
    '''
    
    pass

def radix(list1 = []):

    import random
    import time

    #import clear
    try:
        from apps.menu import menu

        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    #clear screen
    clear()

    #radix menu
    print("--------------------------")
    print("Radix Sort")
    print("--------------------------")

    if list1 == []:
        
        #get input
        list0 = []
        list_length = int(input("Input List Length: "))

        #generate numbers
        for i in range(0, list_length):
            list0.append(random.randint(0,10000))
    else:
        list0 = list1

    #print unsorted list
    print("-------------")
    print("Unsorted List:",list0)
    print("-------------")

    #get maximum number
    try:
        max_num = max(list0)
    except:
        print("--------------------------")
        print("Type Error.")
        time.sleep(5)
        menu()


    #counting sort to sort numbers by index
    place = 1
    while max_num // place > 0:
        counting_sort(list0, place)
        place *= 10
    
    #print sorted list
    print("Sorted List:",list0)
    print("-------------")
