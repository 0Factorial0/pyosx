def bubble(list1 = []):

    import random

    #import clear
    try:
        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    #clear screen
    clear()

    #get input
    print("--------------------------")
    print("Bubble Sort")
    print("--------------------------")

    if list1 == []:

        #set variables
        list0 = []
        isSwapped = False

        list_length = int(input("Input List Length: "))

        #generate numbers
        for i in range(0, list_length):
            list0.append(random.randint(0,100))
    else:
        list0 = list1

    #print unsorted list
    print("-------------")
    print("Unsorted List:",list0)
    print("-------------")

    #check all numbers
    n = len(list0)
    for i in range(n-1):
        #check unsorted numbers
        for j in range(0, n-i-1):
            #find the number array[x] bigger than array[x+1]
            if list0[j] > list0[j+1]:
                #swap numbers
                (list0[j],list0[j+1]) = (list0[j+1],list0[j])
                #set swapped variable true
                isSwapped = True
        #if nothing to swap then break the loop
        if isSwapped == False:
            break
    #print sorted list
    print("Sorted List:",list0)
    print("-------------")
