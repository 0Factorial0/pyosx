def monthy_hall_game():
    
    import time
    import random

    try:
        from apps.menu import menu

        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    #clear screen
    clear()
    
    #print menu
    print("--------------------------")
    print("Welcome To The Monthy Hall Game!")
    print("--------------------------")
    print("Press 1 For Door Number 1")
    print("Press 2 For Door Number 2")
    print("Press 3 For Door Number 3")
    print("--------------------------")
    print("Press 0 For Main Menu")
    print("--------------------------")

    #get input and check errors
    try:
        userinput0 = int(input("Input: "))
        if userinput0 == 0:
            menu()
        elif userinput0 < 0 or userinput0 > 3:
            print("--------------------------")
            print("Input Error.")
            time.sleep(3)
            monthy_hall_game()
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(3)
        monthy_hall_game()

    #calculate the game
    winner = random.randint(0,2)
    door_list = []
    opening_doors = []
    opening_door = 0
    user_door = userinput0-1
    user_door0 = user_door
    
    #input doors
    for i in range(0,3):
        state = "Goat"
        if i == winner:
            state = "Bugatti"
        door_list.append(state)
    #decide opening door
    for j in range(0,3):
        if j == userinput0-1 or door_list[j] == "Bugatti":
            pass
        else:
            opening_doors.append(j)
    if len(opening_doors) == 2:
        opening_door = opening_doors[random.randint(0,1)]
    elif len(opening_doors) == 1:
        opening_door = opening_doors[0]
    else:
        print("--------------------------")
        print("Unexpected Error.")
        time.sleep(2)
        monthy_hall_game()

    #clear screen
    clear()
    
    #opening one door
    print("--------------------------")
    print("Opening One Random Door!")
    print("--------------------------")
    time.sleep(2)
    print("Opening The Door Number {0}!".format(opening_door+1))
    time.sleep(2)
    print("It Has A Goat In It!")
    print("--------------------------")
    time.sleep(2)
    print("Congrats, You Continue The Game!")
    print("--------------------------")
    print("Please Wait For 10 Seconds...")
    print("--------------------------")

    #clear screen
    time.sleep(10)
    clear()

    #remaining doors
    print("--------------------------")
    print("Remaining Doors;")
    print("--------------------------")
    if opening_door != 0:
        print("Door Number 1 ", end="")
        if userinput0 == 1:
            print("[Your Door]", end="")
        print("")
    if opening_door != 1:
        print("Door Number 2 ", end="")
        if userinput0 == 2:
            print("[Your Door]", end="")
        print("")
    if opening_door != 2:
        print("Door Number 3 ", end="")
        if userinput0 == 3:
            print("[Your Door]", end="")
        print("")
    
    #change or stay
    print("--------------------------")
    print("Change Your Door Or Stay")
    print("--------------------------")
    print("Press 1 For Change")
    print("Press 2 For Stay")
    print("--------------------------") 
    
    #get input and check errors
    try:
        userinput1 = int(input("Input: "))
        if userinput1 < 1 or userinput1 > 2:
            print("--------------------------")
            print("Input Error.")
            time.sleep(3)
            monthy_hall_game()
    except:
        print("--------------------------")
        print("Input Error.")
        time.sleep(3)
        monthy_hall_game()

    #clear screen
    clear()

    #calculate solution
    change_door = 0
    for k in range(0,3):
        if k == opening_door or k == user_door:
            pass
        else:
            change_door = k

    if userinput1 == 1:
        user_door = change_door
    elif userinput1 == 2:
        user_door = user_door
    else:
        print("--------------------------")
        print("Unexpected Error.")
        time.sleep(3)
        monthy_hall_game()

    #print solution
    print("--------------------------")
    if userinput1 == 1:
        print("You Decided To Change Your Door!")
    elif userinput1 == 2:
        print("You Decided To Stay With Your Door!")
    time.sleep(2)
    print("And!")
    time.sleep(2)
    if door_list[user_door] == "Bugatti":
        print("You Win A Bugatti!")
    elif door_list[user_door] == "Goat":
        print("You Win A Goat.")
    print("--------------------------")
    print("Please Wait For 5 Seconds...")
    print("--------------------------")

    time.sleep(5)
    monthy_hall_game()