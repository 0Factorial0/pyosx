def menu():

    import os
    import time

    try:
        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")
    
    #clear screen
    clear()

    #get the current location of /python folder
    isRegistered_file_name = "isRegistered.txt"
    file_path = os.path.dirname(os.path.abspath(isRegistered_file_name))

    #main menu / routing
    print("--------------------------")
    print("Select Program Type")
    print("--------------------------")
    print("Press 1 For Internal Apps")
    print("Press 2 For External Apps")
    print("--------------------------") 

    #get input
    try:
        program_type = str(input("Input: "))
    except:
        print("--------------------------") 
        print("Unexpected Error.")
        time.sleep(2)
        menu()

    #check input
    if program_type == "1":
        internal_apps_menu()
    elif program_type == "2":
        external_apps_menu()
    else:
        print("--------------------------") 
        print("Wrong Input.")
        time.sleep(2)
        menu()

#internal apps menu
def internal_apps_menu():

    import time
    import os

    try:
        from apps.internal_apps.calculator import calculator
        from apps.internal_apps.clock import clock
        from apps.internal_apps.notepad import notepad
        from apps.internal_apps.stopwatch import stopwatch
        from apps.internal_apps.settings_menu import settings_menu

        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    #clear screen    
    clear()

    #internal programs menu
    print("--------------------------")
    print("Internal Programs Menu")
    print("--------------------------")
    print("Press 1 For Calculator")
    print("Press 2 For Clock And Calendar")
    print("Press 3 For Notepad[WIP]")
    print("Press 4 For Stopwatch")
    print("Press 5 For Settings")
    print("--------------------------")
    print("Press 0 For Main Menu")
    print("--------------------------") 
        
    #get input
    try:
        internal_type = str(input("Input: "))
    except:
        print("--------------------------") 
        print("Unexpected Error.")
        time.sleep(2)
        internal_apps_menu()

    #check input and route
    if internal_type == "1":
        calculator()
    elif internal_type == "2":
        clock()
    elif internal_type == "3":
        notepad()
    elif internal_type == "4" and os.name == "nt":
        stopwatch()
    elif internal_type == "5":
        settings_menu()
    elif internal_type == "0":
        menu()
    else:
        print("--------------------------")
        print("Input Error.")
        time.sleep(2)
        internal_apps_menu()

def external_apps_menu():

    import time
    import os

    try:
        from apps.external_apps.coinflip import coinflip
        from apps.external_apps.guess_the_number import guess_the_number
        from apps.external_apps.rock_paper_scissors import rock_paper_scissors
        from apps.external_apps.math_game import math_game
        from apps.external_apps.monthy_hall_game import monthy_hall_game
        from apps.external_apps.collect_the_food import collect_the_food

        from apps.external_apps.wish_you_not_start import wish_you_not_start
        from apps.external_apps.platform import platform

        from apps.external_apps.ai_fight import ai_fight
        from apps.external_apps.sorting import sorting

        from osx.systemx.additional_functions.clear import clear
    except:
        print("Import Error.")

    #clear screen    
    clear()

    #external programs menu page
    print("--------------------------")
    print("External Programs Menu")
    print("--------------------------")
    print("Experiments:")
    print("--------------------------")
    print("Press 1 For A.I. Fight")
    print("Press 2 For Sorting Algorithms")
    print("--------------------------")
    print("Mini Games:")
    print("--------------------------")
    print("Press 3 For Coinflip")
    print("Press 4 For Guess The Number")
    print("Press 5 For Math Game")
    print("Press 6 For Rock Paper Scissors")
    print("Press 7 For Monthy Hall Game")
    print("Press 8 For Collect The Food Game")
    print("--------------------------")
    print("Games:")
    print("--------------------------")
    print("Press 9 For Wish You Not[WIP]")
    #to-do name of platform
    print("Press 10 For %NAME OF PLATFORM%")
    print("--------------------------")
    print("Press 0 For Main Menu")
    print("--------------------------") 

    #get input
    try:
        external_type = str(input("Input: "))
    except:
        print("Unexpected Error.")
        time.sleep(2)
        menu()

    #check input and route
    if external_type == "1":
        ai_fight()
    elif external_type == "2":
        sorting()
    elif external_type == "3":
        coinflip()
    elif external_type == "4":
        guess_the_number()
    elif external_type == "5":
        math_game()
    elif external_type == "6":
        rock_paper_scissors()
    elif external_type == "7":
        monthy_hall_game()
    elif external_type == "8" and os.name == "posix":
        collect_the_food()
    elif external_type == "9":
        wish_you_not_start()
    elif external_type == "10":
        platform()
    elif external_type == "0":
        menu()
    else:
        print("--------------------------")
        print("Input Error.")
        time.sleep(2)
        external_apps_menu()